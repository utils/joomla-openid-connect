<?php

/**
 * @package     Joomla.Site
 * @subpackage  com_openidconnect
 *
 * @copyright   2023 Jiří Antoňů & CZ.NIC
 * @license     GNU General Public License version 3 or later; see LICENSE
 */

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

use Joomla\CMS\Factory;

/**
 * OpenID Connect Component Controller
 *
 * @since  0.0.1
 */
class OpenIDConnectController extends JControllerLegacy
{
    private $component_uri = 'index.php?option=com_openidconnect';

    function display($cacheable = false, $urlparams = array())
    {
        $app = JFactory::getApplication();
        $params = $app->getParams('com_openidconnect');

        $base_url = JUri::base();

        // Get the code from URL params
        $code = Factory::getApplication()->input->get('code', '', 'string');

        if (empty($code)) {
            echo "Authentication failed - not code was provided.";
            return;
        }

        // Make request to token endpoint
        $token_request = curl_init();
        $token_endpoint = $params->get('oidc_token_url');
        curl_setopt($token_request, CURLOPT_URL, $token_endpoint);
        curl_setopt($token_request, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($token_request, CURLOPT_POST, true);

        $post_params = array(
            'grant_type' => 'authorization_code',
            'code' => $code,
            'redirect_uri' => $base_url . $this->component_uri
        );

        curl_setopt($token_request, CURLOPT_POSTFIELDS, http_build_query($post_params));

        curl_setopt($token_request, CURLOPT_HTTPHEADER, ["Authorization: Basic " . base64_encode($params->get('client_id') . ":" . $params->get('client_secret'))]);

        $token_result = curl_exec($token_request);
        $token_object = json_decode($token_result);

        if (curl_errno($token_request)) {
            JLog::add('curl error: ' . curl_error($token_request), JLog::ERROR, 'openid-connect');
            echo "Authentication failed at token request.";
            return;
        }
        curl_close($token_request);

        // Make request to userinfo endpoint
        $userinfo_request = curl_init();
        $userinfo_endpoint = $params->get('oidc_userinfo_url');
        curl_setopt($userinfo_request, CURLOPT_URL, $userinfo_endpoint);
        curl_setopt($userinfo_request, CURLOPT_RETURNTRANSFER, 1);

        curl_setopt($userinfo_request, CURLOPT_HTTPHEADER, ["Authorization: Bearer " . $token_object->access_token]);

        $userinfo_result = curl_exec($userinfo_request);
        $userinfo_object = json_decode($userinfo_result);

        if (curl_errno($userinfo_request)) {
            JLog::add('curl error: ' . curl_error($userinfo_request), JLog::ERROR, 'openid-connect');
            echo "Authentication failed at userinfo request.";
            return;
        }
        curl_close($userinfo_request);

        // Find out whether user already exists
        $db = JFactory::getDbo();
        $query = $db->getQuery(true)
            ->select($db->quoteName('id'))
            ->from($db->quoteName('#__users'))
            ->where($db->quoteName('email') . ' = ' . $db->quote($userinfo_object->email));
        $db->setQuery($query, 0, 1);

        $existing_user = $db->loadResult();

        // Log out any previous user
        $app = JFactory::getApplication();
        $app->logout();

        // Find user if exists, or create a new one
        if ($existing_user) {
            $user = JFactory::getUser($existing_user);
        } else {
            $user = new JUser;
            $user_data = array(
                "username" => $userinfo_object->email,
                "name" => $userinfo_object->name,
                "email" => $userinfo_object->email,
                "block" => 0,
                "activated" => 1,
                "is_guest" => 0
            );

            $user->bind($user_data);
            $user->save();

            $db->setQuery('SELECT id FROM #__usergroups' . ' WHERE LOWER(title) LIKE \'registered\'');
            $groups = array($db->loadObject()->id);

            JUserHelper::setUserGroups($user->id, $groups);
        }

        // Log user in
        JPluginHelper::importPlugin('user');
        $options = array();
        $options['action'] = 'core.login.site';
        $response = new stdClass();
        $response->username = $user->username;
        $response->language = '';
        $response->email = $user->email;
        $response->password_clear = '';
        $response->fullname = '';
        $app->triggerEvent('onUserLogin', array((array)$response, $options));

        // Redirect home
        $this->setRedirect($params->get('after_login_redirect_url') ?: "/");
        return;
    }

    function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[random_int(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    function login()
    {
        $app = JFactory::getApplication();
        $params = $app->getParams('com_openidconnect');
        $base_url = JUri::base();
        $client_id = $params->get('client_id');
        $response_type = 'code';
        $this->setRedirect($params->get('oidc_authorization_url') .
            '?client_id=' . $client_id .
            '&response_type=' . $response_type .
            '&redirect_uri=' . urlencode($base_url . $this->component_uri) .
            '&state=' . $this->generateRandomString() .
            '&scope=' . $params->get('scope') .
            ($params->get('require_prompt') ? '&prompt=consent' : ''));
        return;
    }
}
