# Joomla OpenID Connect Module

This is an OpenID Connect module for the [Joomla CMS](https://www.joomla.org/). It was developed by [CZ.NIC](https://nic.cz) to be used with [MojeID](https://www.mojeid.cz), but it supports any OpenID Connect identity provider.

It is intended to authenticate end users.

## Documentation
This module can be easily installed using Joomla's *Extension Installer* - packaged plugin releases can be downloaded [here](https://gitlab.nic.cz/utils/joomla-openid-connect/-/releases/permalink/latest). Full documentation, including installation instructions and configuration, is available [here](https://www.mojeid.cz/dokumentace/html/ImplementacePodporyMojeid/OpenidConnect/KnihovnyModuly/joomla.html).
